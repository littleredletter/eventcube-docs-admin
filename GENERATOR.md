## Documentation Generator
HTML Documentation can be generated using the [`couscous/couscous`](https://packagist.org/packages/couscous/couscous) package.

### Installation
Download `couscous.phar`, make it executable and move it to your bin directory so it can be triggered system wide using the `couscous` command.

```sh
curl -OS http://couscous.io/couscous.phar
chmod +x couscous.phar
sudo mv couscous.phar /usr/local/bin/couscous
```

### Running
Once installed, run the generate command in the documentation's root directory _(beside `couscous.yml`)_ to compile the documentation into the `/build` directory which you can use as the document root of a virtualhost.

```sh
couscous generate --target=./build
```

> **Note:** `couscous generate` should be run each time the documentation is updated.
