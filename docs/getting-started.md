# Getting Started

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Dependencies](#dependencies)
- [Git](#git)
- [Site structure](#site-structure)
- [Installation](#installation)

## Introduction
The Eventcube system is separated into two applications; **Eventcube Admin** and **Eventcube Front**. Both applications run on [`Laravel v4.1`](https://packagist.org/packages/laravel/framework#4.1.x-dev).

Official documentation is no longer available online for version `4.1` though they can be forked from the [documentation repo](https://github.com/laravel/docs/tree/4.1).

> **Note:** The Laravel framework differs significantly from Laravel 4.2 onwards and cannot be directly upgraded.
> Please refer to the [official documentation](http://laravel.com/docs/5.1/upgrade) before upgrading this application.

## Requirements
* PHP _(& PHP-CLI)_ >= 5.5.9
* MySQL => 5.5.40
* PHP Extensions
    * MCrypt
    * GD
    * Curl

## Dependencies
The applications use [Composer](http://getcomposer.org/) to manage package dependencies.

Where available, dependencies have been set to track minor versions within `composer.json`.

## Git
This software is version controlled using Git and follows the [Gitflow workflow](http://nvie.com/posts/a-successful-git-branching-model/).

New features are branched from `develop` and worked on under a `feature` branch, e.g. `feature/EC109`, `feature/bulk-store-editing`. Once complete, they are merged back into `develop` ready to be included in the next release branch.

Hotfixes are branched from `master` and nested according to their type, e.g: `hotfix/patch/copy-change`. Once complete they are merged into `master` and `develop` and deployed immediately.

## Site structure
The applications both follow the standard Laravel 4.1 directory structure.

### Application Core `/app`
The core contains all of the controllers, models, views, configuration, libraries, etc.

### Config and Environment `/app/config`
Configuration files are split out into component parts. They can be accessed using the config facade and dot notation as the attribute, e.g. `Config::get('rsn.uploads.tmp')`, `Config::get('app.timezone')`.

Configurations can be overridden on an per-environment basis by creating a directory using the environment name, e.g. `/app/config/local`. Configurations within this directory are merged with the base configurations allowing you to override single properties.

The environment is determined by machine hostname and can be configured in `/bootstrap/start.php`.

> **Note:** Unless overridden the default environment is `production`.

### Templates `/app/views`
Templates are written in [Twig](http://twig.sensiolabs.org/) syntax.

The [`rcrowe/twigbridge`](https://github.com/rcrowe/TwigBridge/tree/0.6) package is used to provide Twig support and can be configured in `/app/config/packages/rcrowe/twigbridge/config.php`.

### Document Root `/public`
The public directory contains all forward-facing assets including CSS and JavaScript. Event flyers, store logos, mailer and feature images uploaded to `/public/uploads`.

> **Note:** There is a currently a CDN mirror pointing to this location.

## Installation
The installation process is similar for both applications.

### Clone Application
```sh
# Admin
git clone git@github.com/eventcube/eventcube-admin

# Front
git clone git@github.com/eventcube/eventcube-front
```

> **Note:** The virtual host should use the `/public` directory as the document root of each application.

### Install Dependencies
Change into the root directory of one of the applications and install Composer's dependencies.
```sh
cd eventcube-admin
composer install
```
> **Note:** Do not run `composer update` on the production server. This process updates every package to the latest version potentially breaking functionality. It also is extremely resource heavy and can take a number of minutes to complete.

### Check Configuration
Below are a list of the primary configuration files and properties to verify for each application.

#### Eventcube Front

#### **`app/config/app.php`**
Configuration relating to the core application including enabling `debug` mode. Service providers are registered in `providers`, and Facades in `aliases`.

The `key` property is used by the encryption service and **must** be the same on the Eventcube Admin application. Modifying the current value will prevent any encrypted data from being decrypted. `salt` is used as a simple `md5()` hash salt (using `Helper::salty()`) and must also be the same on Eventcube Admin.

#### **`/app/config/cache.php`**
Eventcube Front uses the `memcached` driver for caching. Memcached server details should be configured in the `memcached` property.

#### **`/app/config/database.php`**
Laravel supports several RDBMS database drivers, Eventcube uses MySQL. Server details are configured in `connections.mysql` and `default` should be the name of the connection (`mysql`).

#### **`/app/config/mail.php`**
Email is sent via the Mandrill, see the [email documentation](basics.md#email) for further details. The `from` properties are overridden with the current Store's settings.

#### **`/app/config/queue.php`**
Queues are managed by Amazon SQS, see the [queue documentation](basics.md#queues) for further details. The server details should be configured in `connections.sqs` and `default` should be the name of the connection (`sqs`).

#### **`/app/config/rsn.php`**
The RSN configuration contains application specific settings. Several of these properties are overridden by the current Store's settings.

The `uploads` properties contains the paths (relative to the Eventcube Admin document root) for uploaded images including event flyers and store feature banners.

`themes_dir` is the path (relative to the Eventcube Front document root) for theme specific files including css overrides and other static assets. See the [theming documentation](basics.md#theming) for further details.

`cache_breaker` should be updated with a unix timestamp every time primary assets have been updated to force a browser cache refresh.

Primary and Secondary `colours` are used by themes and can be overridden in Store settings. They allow Stores using the base theme to bring the colour of their store in line with their branding.

`images` set out the image dimensions used for Event flyers as well as Promoter and Store logos. Cropping is performed on the Eventcube Admin application, and the settings on each application should be kept in sync.

`rsn_tickets` is overridden by the Store's URL setting. `rtpa` should point to the Eventcube Admin application.

`cdn` is the CDN used for uploaded assets. The CDN points to the `/public/uploads` directory on Eventcube Admin.

`static` is a CDN for static assets including css, javascript and image files. The CDN points to the `/public` directory on Eventcube Front.

Email addresses stored in `email` are overridden by the current Store.

Delivery types for physical tickets sent By Post are configured in `delivery`. See [delivery documentation](orders.md#delivery) for further details. The `break` property is used as a cost multiplier. Additional delivery types can be added to the `costs` array.

`gateway` information is overridden by the current Store.

#### Eventcube Admin
The same configuration properties verified on Eventcube Front should also be verified on Eventcube Admin with a number of exceptions.

##### `/app/config/app.php`
`url` must be set to the Eventcube Admin URL. PDF documents are generated by the CLI, as such the application runs without a URI. The PDF generator must have a base URL to load external imagery.

#### **`/app/config/cache.php`**
Eventcube Admin does not currently use Memcached and should use the `file` driver.

##### `/app/config/mail.php`
The `from` array is used by emails sent by the Eventcube system to Store Owners, Promoters etc — generally not to ticket customers.

##### `/app/config/queue.php`
Eventcube admin is the queue worker so it is important to ensure the connection details are correct.

##### `/app/config/rsn.php`
The application specific configuration settings are much the same as Eventcube Front. The array of `supported_gateways` contains the properties required for making a connection to each gateway. The `rules` and `messages` fields are used by an instance of `Illuminate\Validation\Validator`.

[Third party](integration.md) API keys and configurations are also set in this configuration.

### Migrate Database
The following migration must be run to generate the database schema.

```sh
php artisan migrate --package=cartalyst/sentry
php artisan migrate
```

> **Note:** Migrations should *only* be run on *Eventcube Admin*.

### Maintenance
Both applications can be put into maintenance mode using `php artisan up` and restored using `php artisan down`.

Clearing caches can be performed with `php artisan cache:clear`

Twig templates should be cleared after each template deployment with `php artisan twig:clean`. There have previously been issues with server permissions and Twig caches. It is advisable to run the cach clearing twice, once as `sudo` and once as the current user: `sudo php artisan twig:clean && php artisan twig:clean`.

Autoloading and optimisation files can be refreshed with `composer dumpautoload && php artisan optimize` however these are run automatically after running `composer install`.

> **Note:** It may be necessary to run any of these commands as `sudo`.

### Generate Dummy Data
Eventcube Admin has a command to generate dummy data; users, stores, promoters, events, tickets and orders.


```sh
php artisan generate:dummy
```

> **Warning:** If `'y'` is chosen when prompted to rollback migrations, **all** data in the database will be deleted.

> **Note:** The command will only run in the `local` environment. To force it to run in an alternative environment, use the `--force` flag.
