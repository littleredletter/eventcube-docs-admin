# Third-party Integration

- [Payment Gateways](#payment-gateways)
- [Stripe](#stripe)
- [QiQ Ticket Scanning](#qiq)
- [Evvnt Distribution](#evvnt)

## Payment Gateways
Eventcube makes use of the [`omnipay/common`](https://packagist.org/packages/omnipay/common#v2.3.4) package to handle payment processing. Omnipay is extended using official drivers ([`omnipay/sagepay`](https://packagist.org/packages/omnipay/sagepay)) as well as customised drivers written specifically for Eventcube ([`steveneaston/worldpay`](https://bitbucket.org/steveneaston/worldpay), [`steveneaston/stripe`](https://bitbucket.org/steveneaston/stripe)) which offer additional functionality not provided by the core drivers.

## Stripe
[Stripe](https://stripe.com) is used for several tasks across Eventcube Admin and Eventcube Front and makes use of the `steveneaston/stripe` package for Omnipay as well as the [`stripe/stripe-php`](https://packagist.org/packages/stripe/stripe-php#v1.18.0) package for direct API functionality.

### Stripe Connect
[Stripe Connect](https://stripe.com/connect) allows Eventcube to deduct Store fees directly during order processing. Stores can connect their account when they set up their plan.

When payments via _Stripe Connect_ a [Token](https://stripe.com/docs/api#tokens) is created using the customer's card details before a _Charge_ can be made. This differs from a regular _Stripe Charge_ where card details are sent along with the request.

> **Note:** If a Store has their _Stripe_ API Key set manually by a Super Admin, fees are not deducted automatically.

### Admin Payments
Payments are also taken by Eventcube Admin for features such as [QiQ](#qiq) and [Evvnt](#evvnt). _Stripe_ is the only payment gateway used on Eventcube Admin.

### Charge Retrieval
Card fees, location and fingerprints are retrieved directly via the _Stripe_ API after an order has been placed. See [Queued Events](orders.md#queued-events) for further details about this data and how it is used.

> **Note:** The API version is set within the Stripe's Account Settings (under API Keys) and can be updated to the latest version as required. Please refer to the [changelog](https://stripe.com/docs/upgrades) before upgrading.

## QiQ
[QiQ](http://www.qiqit.co.uk/) is a ticket scanning and guest list management application. Internal _(Eventcube Events)_ and External Barcodes _(Ticketmaster, Eventbrite etc.)_ are synchronised using the `qiq:deploy` Artisan Command and uses the `ThirdParty\QiQDeployment` class.

Deployment is triggered by several events including `qiq.event` _(deploys barcodes for an Event)_ and `qiq.order` _(deploys barcodes for an individual Order)_.

### Internal Barcodes
Internal [barcodes](orders.md#barcodes) are uploaded during the [order queue](orders.md#queued-events) event and whenever an Order is updated.

> **Note:** Each unique barcode is uploaded to _QiQ_ — rather than using a barcode and quantity relationship — as such there may be multiple barcodes in _QiQ_ relating to a single a customer.

Order and Ticket status are kept up to date in _QiQ_. If an Order status is set to `pending` or `cancelled`, _QiQ_ adds a message to each barcode relating to the Order under _QiQ's_ `blockedReason` property. If the order status is set back to `ok`, the block is removed.

Revoked tickets are also synchronised using `blockedReason`. If there is a reason given by the Store Admin or Super Admin, this will be added otherwise the property will be `"Revoked"`.

### External Barcodes
Eventcube allows Stores and Promoters to upload barcodes and guestlists from third party ticketing platforms. The customer data is parsed from a `.csv` file and a new _QiQ_ "list" is created to contain each uploaded list.

> **Note:** Guestlists can be uploaded _without_ barcodes but must have a first and last name.

### Quotas
_QiQ_ quotas are set per-**Event**. Quotas can be enabled/disabled per-**Promoter** for [Internal](#internal-barcodes), [External](#external-barcodes) or both barcode sources within `Edit Promoter`. Both Internal and External barcodes are deducted from the same quota.

Promoter quota flags are stored in the `qiq` meta key with values `true : Paid for` and `false : Free`:

``` json
{
  "external": true,
  "internal": false
}
```
> This Promoter must pay to upload barcodes from external applications. Eventcube barcodes are free.

Promoters can purchase a _"bucket"_ of credits, with each credit representing a single unique barcode.

When a Promoter's `trusted` meta key is enabled _(Promoter Settings > General > "Allow paying with funds")_, they can purchase _QiQ_ quota using any funds owed to them from previously ended events. If they have insufficient funds they must use a credit or debit card.

> **Note:** QiQ quotas are disabled by default for all Promoters.

### Metadata
_QiQ_ meta for an Event is stored with the `qiq` key.

```json
{
  "active": true,
  "bucket": 0,
  "external": 0,
  "internal": 251,
  "lists": {
    "default": {
      "count": 0,
      "id": "bcb0fbcd-9a7c-4252-99a2-8245cd3dfb49",
      "name": "Eventcube"
    }
  },
  "qiq_id": "463a6d8d-f301-4bfa-8cff-aedd578a827a",
  "updated_at": "2014-07-16 18:20:43"
}
```

* `active` allows Store Admins or Promoters to switch QiQ integration on or off for an individual event.
* `bucket` lists the Event's current _QiQ_ quota. This is overridden if they do not pay for either [Internal](#internal-barcodes) or [External](#external-barcodes) barcodes.
* `external` and `internal` lists the number of barcodes already synchronised with QiQ.
* `lists` stores the _QiQ_ generated UUID given to each _list_, along with it's name and tally.
* `qiq_id` is the _QiQ_ generated UUID for the _event_.

## Evvnt
[Evvnt](https://www.evvnt.com/) distributes an event to several social event listing sites and provides metrics for the Promoter.

> **Note:** _Evvnt_ charges can be enabled/disabled per-**Store** _(Stire Settings > Administration > "Pays for Evvnt")_ and is on by default.

### Metadata
_Evvnt_ meta is stored for an Event using the `evvnt` meta key.

#### Before Payment
Promoters must first complete some information before their event can be distributed by _Evvnt_. The event description is required and they must select which _Evvnt_ category their event fits into. Promoters also have the choice of showing individual ticket prices.

```json
{
  "data": {
    "category": "5",
    "email": "hello@eventcube.io",
    "name": "Eventcube",
    "tel": "01234567891",
    "tickets": {
      "1234": "on"
    }
  },
  "paid": false
}
```

Before _Evvnt_ distribution has been paid for, the `paid` flag will be false.

#### After Distribution
Once an event has been distributed, _Evvnt_ assigns the event an `evvnt_id` which is stored in the Event `qiq` meta key for refreshing metrics and for updating distribution information.

```json
{
  "created_at": "2015-08-28 12:16:18",
  "evvnt_id": 112233,
  "tickets": [
    "1234"
  ],
  "updated_at": "2015-08-28 12:16:18"
}
```

