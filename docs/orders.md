# Orders

- [Reference](#order-reference)
- [Barcodes](#barcodes)
- [Status](#order-status)
  - [Fraud](#fraudulent-orders)
- [Delivery](#delivery)
- [Tickets](#tickets)
- [Totals](#totals)
- [Resellers](#reseller-orders)
- [Discounts](#discounts)
- [Queued Events](#queued-events)


## Introduction
Data for orders is stored across 4 tables

* `orders`: Reference, customer details, transaction data, status
* `order_ticket`: Cached ticket information, order quantity, status
* `order_totals`: Tallied totals for all tickets on the order
* `order_reseller`: Reseller store quantities

## Order Reference
Order references are generated using the `Helper::generateOrderId()` function.

All order references follow the same pattern: `TS-[string(4)]-[int(10)]`. The string consists of 4 upper-case alpha characters, avoiding ambiguous letters (`I`, `O`, `L`, `D`).

The integer consists of a 10 digit unix timestamp with the leading 3 digits are removed. The timestamp is then reversed and prefixed with a random integer between `100–999`.

## Barcodes
Barcodes are generated _per **ticket**, per **order**_. They are not persisted and are generated on-the-fly.

Barcodes are numeric, 10 digits in length and generated with the pattern `[ticket_id][reference_digits][multiplier]`

The number of digits extracted from the reference is dependent on the length of the `multiplier` and `ticket_id`.

An order with reference `TS-HSAB-7571296699` has two ticket tiers (IDs: `1234`, `5678`).

* **3** tickets ordered for ticket `1234`
    * `1234757121`
    * `1234757122`
    * `1234757123`
* **2** tickets ordered for `5678`
    * `5678757121`
    * `5678757122`

If there had been **30** tickets purchased for ticket `5678`, the final barcode would be: `5678757130`

```php
$order = Order::where('reference', 'TS-HSAB-7571296699')->first();

// Loop through all items on the order
foreach ($order->items as $item) {
    // Generate a unique barcode up to the quantity of each ticket
    for ($i = 1; $i <= $item->quantity; $i++) {
        echo $order->barcode($item->ticket, $i);
    }
}

// Response:
// 1234757121
// 1234757122
// 1234757123
// 5678757121
// 5678757122
```

### Barcode Override
The function `barcode()` should never be called directly from a Ticket model, instead the ticket should be passed into the `barcode()` function from the Order model.

Barcodes can be overridden for an individual order should there be a need for it. This allows the storage of a meta entry against the Order with the key `barcode`. This meta entry will be passed into the barcode function instead of the the order reference.

## Order Status
Orders may have one of three statuses:

- `ok`: Order is valid.
- `pending`: Order is _In-Limbo_ and is generally awaiting action from Store Admin or Super Admin. Tickets remain allocated to the customer.
- `cancelled`: Order has been cancelled. Tickets are no longer allocated to the customer.

> **Note**:  Customers can only print downloadable tickets with the Order status is `ok`.

### Fraudulent Orders
Orders marked as _Fraudulent_ have been done so automatically via the [post-order Event](#queued-events). The data for fraudulent orders is stored in the `fraud` meta key.

> **Note:** Fraud checking is currently only performed on orders made via the [Stripe](integration.md#stripe) gateway.

#### Country Specific
Cards registered in the following countries are automatically flagged as fraudulent, and the order `status` set to `pending`.

* `US : United States of America`
* `BR : Brazil`

#### Stripe Fingerprint
Stripe returns a fingerprint attached to the [Card object](https://stripe.com/docs/api#card_object) within the [Charge object](https://stripe.com/docs/api#charge_object).

When a customer places more than one order containing tickets for the _same_ event, the order is flagged as fraudulent, and the order `status` set to `pending`.

The `fraud.orders` meta property contains all of the customer's orders containing tickets for the matched event where the same card fingerprint was used.

The `fraud.name` meta property contains a boolean. If the shipping names are different across the matched orders, the boolean is `true`.

> **Note**: Fingerprint checking is only performed on orders over `£25.00`.


## Delivery
Delivery of physical tickets is defined by delivery **method** and **type**.
### Delivery Method
Delivery method is either `By Post` or `Collection`.

The delivery method may be different per Ticket on the Order. Tickets can be added as `By Post Only`, `Collection Only` or `Either`, the latter giving the customer a choice of delivery method.

### Delivery Type
Delivery type is set if there are tickets purchased with the `By Post` delivery method. Delivery type is set _per *Order*_ with the view all physical tickets will be posted in one delivery.

Delivery types are set in config: `rsn.delivery.costs`, each having it's own cost associated (e.g. Standard - £1.99, Next Day - £3.50)

Delivery costs are multiplied according to the delivery breakpoint setting (config: `rsn.delivery.break`) using the calculation: `ceil(quantity / break)`.

## Tickets
Order ticket data is stored in the `order_ticket` pivot table. Each Ticket within an order is stored with it's information at the time of purchase, including _title_, _booking_ fee and price. Storing this data allows stores to alter ticket titles, prices and booking fees without affecting already completed orders. It also allows for custom ticket prices per-order.

Each Order Ticket entry has an `opened` flag which is set to `1` when the customer has viewed their downloadable tickets.

Order Tickets may have one of three statuses;

- `ok`: Only tickets with this status should be allowed entry into an Event.
- `revoked`: This ticket has been revoked. The order may still have other valid tickets.
- `instalment`: Ticket was purchased as an instalment ticket and has yet to complete payment.

Physical tickets ordered with the `By Post` delivery type have the `shipping` flag set to `1`.

> **Note:** See [Ticket Discounts](#ticket-discounts) for further information regarding use of the `discount` column.

## Totals
Order totals data is stored in the `order_totals` pivot table. These columns contain the tallied amounts for all Tickets within an Order.

### Updating Totals
The Order model's `updateTotals()` function is invoked whenever tickets are added, amended or revoked. This recalculates the totals for the order, it **does not** charge or refund money. The only value not updated during this operation is `card_fee`, this is set at the time of order. All other values (**including** `system_fee`) are recalculated.

> **Note:** The value of any revoked tickets still count towards the order total. If money has been refunded, the quantity order ticket should be reduced or the order status set to `cancelled`.

## Reseller Orders
When an Order has been placed through a Reseller Store, the Order is completed on the Event's Store.

The Reseller Reward is stored on the `order_reseller` table and is calculated from the number of tickets sold for the Reseller Event based on the agreed Reseller Rate.

## Discounts
There are two types of discount available on Eventcube; per **Ticket** and per **Event**. Each discount has a meta entry against the Order with the key `discount`. Orders may also have more than one `discount` meta entry.

> **Note:** Discounts cannot be applied across an entire order because a single order may consist of more than one Event from more than one Promoter.

All discounts can be retrieved for an order using the function a helper function:

```php
getDiscount( bool $append = true );
```

> **Note:** If `$append` is true the `amount` (for Ticket discounts) and `event` (for Event discounts) properties are added to each discount object.

### Ticket Discounts
Ticket discounts are primarily used for Group Buy tickets. A Ticket discount meta will deduct the entire ticket price plus booking fee _(at the time of purchase)_. This value is also stored in the `discount` column of the `order_ticket` table.

The `meta_value` for a Group Buy ticket would look like this:

```json
{
  "description": "Group Buy",
  "ticket_id": 1867,
  "type": "group"
}
```

### Event Discounts
Event discounts are primarily used for social discounts. Event discounts differ from Ticket discounts in that a specific discount amount is applied to an individual Event included in an order.

```json
{
  "amount": "0.30",
  "description": "Attend on Facebook",
  "event_id": "1300",
  "service": "facebook_attend",
  "type": "social"
}
```

### Applying Discounts
Discounts will not be applied until `updateTotals()` has been run on the Order entity. The easiest way to run this function is to view the order, click **Allocate Another Ticket** followed by **Update Order**.

> **Note:** Discounts must be added to the meta table manually.

## Queued Events
Methods on the `Queues\Order` class are triggered in the background `60` seconds after a successful order.

### Notifications
`fire()` notifies Event Promoters and Store Admins that sales of particular tickets have either sold out or are nearing completion (`90%` sold). It will also notify them if the entire Event has sold out.

A request to update [QiQ Ticket Scanning](integration.md#qiq) is also triggered at this time.

### Order Flagging
`flag()` runs [fraud checking](#fraudulent-orders) on orders made via the Stripe gateway. This function also retrieves and stores `card` and `application fees`.

**Card fees** for orders made via Stripe are initially set to `£0.00` as they are not returned by the Stripe API during charge authorisation.

**Application fees** are calculated automatically by the system based on the Store's current plan and are updated using exact figures from Stripe if they have connected their gateway via Stripe Connect.
