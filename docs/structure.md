# Model Structure

- [Users](#users-and-permissions)
    - [Roles](#roles)
    - [Reps](#rep)
- [Events](#events)
- [Metadata](#metadata)
    - [Accessing](#retrieving-meta)
    - [Storing](#storing-meta)

## Users and Permissions
Users have a single account across Eventcube but may have access to many Stores and Roles.

There are several global User variables injected into the templates:

* `user`
* `logged_in`
* [`is_promoter`](#promoter)
* [`is_admin`](#store-admin)
* [`is_super`](#super-admin)
* [`is_super_now`](#super-admin)
* [`pseudo_store`](#super-admin)
* [`is_rep`](#rep)

> **Note:** When injecting a User for display (e.g. show/edit User) it should be injected as the variable `_user` so it doesn't override the global `user` variable.

### Roles
Users can have multiple roles and different permissions for each role. The `cartalyst/sentry` package does not offer this functionality out of the box and has been developed for use on **Eventcube Admin**.

```
├── User
    ├── Store One
        ├── Store Admin
    ├── Store Two
        ├── Rep
        ├── Promoter
            ├── Promoter One
            ├── Promoter Two
    ├── Store Three
        ├── Promoter
            ├── Promoter Three
```

Users are added to a pre-defined Sentry Group (currently `store_admin`, `promoter` and `rep`) on a Store-by-Store basis. Group permissions are stored as JSON in the `permissions` column of the `groups` table. Permissions are either set to `1 : Allow` or `0 : Deny`.

The following permissions would allow a user of this Group to **create** and **update** an Event, but not **delete** it.
```json
{
  "event.create": 1,
  "event.update": 1,
  "event.delete": 0
}
```

Users are added to Stores in the `store_user` pivot table and can be added to multiple Stores. A User may also have multiple Roles within a single Store. The permissions for each Group they belong to are inherited from the Group permissions, and can be overridden.

Overriden permissions are set as `-1 : Deny`, `1 : Allow` and `0 : Inherit`.

```json
{
  "groups": {
    "1": {
      "permissions": {
        "event.create": -1,
        "event.update": 0
      }
    },
    "2": {
      "permissions": {
        "ticket.physical": 1,
      }
    }
  }
}
```
In this example, the User has been added to two Groups on a Store; `1 : store_admin` and `2 : promoter`. By default, Store Admins may **create** and **update** Events, Promoters cannot select physical tickets. Here the permissions have been overridden so the user _cannot_ create events in their Store Admin role, they _can still_ update events because this permission is inherited. The user _can_ select physical tickets in their Promoter role.

Permissions can be checked in Templates and Controllers with these methods:

```php
$user = Sentry::getUser();

// Check user can has permission
$user->can('event.create');

// Reverse of can()
$user->cant('event.create');

// Checks user has all of these permissions
$user->can('event.create', 'event.update');

// Checks user has any of these permissions
$user->canAny('event.create', 'event.update');
```

#### Super Admin
Super Admins can be created by adding `{"superuser":1}` to the `permissions` column of the `users` table.

Super Admins have global access to all features, Users and Stores. They can switch into a Store in "pseudo" mode which emulates a Store Admin role. When in Pseudo Store mode, the global template variable `pseudo_store` contains the ID of the Store they are emulating.

Within Templates and Controllers, Super Admins can be checked using these methods:

**`is_super_now` and `Sentry::getUser()->isSuper()`**
Check user is currently active as a Super Admin.

**`is_super` and `Sentry::getUser()->isSuper(true)`**
Check user has Super Admin permissions even when using a different role.

#### Store Admin
Store Admins can also be the owner of a Store, set in the `owner` column of the `store_user` table. Store Owners are the Users that are notified of Store events, e.g. Event requires activation, Reseller invitations. Store Admins can be fetched using the `getAdministrators()` function on a Store model.

Store Admins have access to all Events and Orders within their Store.

#### Promoter
Promoter roles differ slightly from other roles in that the User must first be added to the Promoter Group _then_ added to a Promoter. Promoters are their own entities (like Stores) and may have multiple Users attached to them.

> **Note:** A Promoter is **not** a User.

Any permissions that are overridden for a User's Promoter Role will apply to all of the Promoters they are attached to.

As with Stores, Promoters can have owners, set in the `owner` column of the `promoter_user` table. Promoter Owners can be fetched using the `getOwners()` function on a Promoter model.

Both Promoter Owners and Promoter Admins (`getOwnersAndAdmins()`) are notified of events, e.g. Tickets have sold out, remittance advice.

#### Rep
Rep accounts are different again, they have a separate dashboard and are not tied to a single store and should only have a single Rep Role.

> **Note**: Rep roles must still be attached to a Store, however they can represent any Eventcube Store.

Before a Rep can create their "reppable link" they must first be invited to an Event by an Event Promoter or Store Admin. Store Admins may also make their events available to Eventcube's entire Rep network by toggling on **List event for reps** within `Event > Advanced Settings`.

> **Note**: Events must have at least one reppable Ticket before they will be available to Reps.

Once a Rep creates their reppable link (e.g. `ticket-store.com/rep/dans-super-party`), the identifier (`dans-super-party`) will be stored against any orders placed by customers after visiting the link. The identifier is added to the `rep` column in the `orders` table.

## Events
* **Tickets** belong to a single **Event**.
* **Events** belong to a single **Promoter**.
* **Promoters** belong to a single **Store**.
* **Events** have a single **Venue**.

```
├── Store
    ├── Promoter
        ├── Event
            ├── Ticket
```

**Events** have a single status:
* `incomplete`: created, awaiting tickets
* `submitted`: tickets added, awaiting approval of a Store Admin*
* `ok`: event has been approved
* `paid`: event has completed, monies have been settled with the Promoter
* `hidden`: event is disabled from listings

> _* Events added by a Store Admin are automatically activated. Events added by a Promoter must first be approved._

> **Note:** Events can be activated _without_ tickets by a Store Admin.

### Scheduling Tickets
By default, ticket sales are closed `3` hours before the Event begins (config: `rsn.events.close_event`).

Ticket sales can be extended using `Event > Advanced Settings > Schedule Sales`. This allows tickets to remain on sale past the start of the Event. This can be useful if the event is a week-long festival, or if customers can purchase tickets right up until doors open.

## Metadata
Metadata can be applied to any Model and is stored in the `meta` table. Each meta entry has 5 columns:
* `id`: Primary key
* `meta_id`: Primary key of the associated Model
* `meta_type`: Type of associated Models (User, Store, Event etc)
* `meta_key`: Meta identifier (should be slugified and lowercase)
* `meta_value`: Boolean (`1` or `0`), Integer, String or encoded JSON

Meta can be used to store miscellaneous information or for features that can be enabled/disabled without requiring their own table or column.

A Model's `meta()` relation provides a `MetaCollection` containing `Metas` Models.

> **Note:** User Models have their own Meta relationship using the `UserMeta` class

### Retrieving Meta
```php
$store = Store::find(1);

// Provides: www.ticket-store.com
$store->meta->get('domain');

// get() can return a default fallback in the event no meta is found
// Provides: null
$store->meta->get('biscuits');

// Provides: 'Nothing'
$store->meta->get('biscuits', 'Nothing');
```
```php
// Data stored as JSON:
// meta_key: social
// meta_value: {"twitter":"ticketstore","facebook":"TicketStore"}

// Provides:
object(stdClass)#1 (2) {
    ["twitter"]=> string(10) "ticketstore"
    ["facebook"]=> string(10) "TicketStore"
}
$store->meta->get('social');

// Provides:
array(2) {
    ["twitter"]=> string(10) "ticketstore"
    ["facebook"]=> string(10) "TicketStore"
}
$store->meta->get('social', null, true); // Passing true as the third parameter returns an array

// Dot notation can be used to return a single property
// Provides: 'ticketstore'
$store->meta->get('social.twitter');
```

### Storing Meta
Meta should be stored using using the `Metas::add()` or `Metas::addOrUpdate()` functions.

```php
Metas::add( Model $model , string $key, string $value);
```
Using `add()` creates a new Meta entry even if there is an existing meta key for this Model.

```php
Metas::addOrUpdate( Model $model , string $key, string $value [, int $id = null]);
```
Using `addOrUpdate()` will update an existing Meta entry or create a new entry if it does not already exist.

Dot notation can be used to update individual properties for JSON data
```php
// meta_key: social
// meta_value: {"twitter":"ticketstore","facebook":"TicketStore"}

$store = Store::find(1);
Metas::addOrUpdate($store, 'social.facebook', 'Eventcube');

// Provides: Eventcube
$store->meta->get('social.facebook');
```
