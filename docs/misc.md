# Miscellaneous

- [Naming Conventions](#naming-conventions)
- [Problematic Queries](#problematic-queries)

## Naming Conventions
The RSN naming convention, including the `Rsn\Rsn` service provider, `/app/config/rsn.php` configuration and `rtpa` properties relate to Eventcube's original incarnation, _RSN Tickets Promoter Area_ before it was re-developed into the Eventcube whitelabel application.

## Problematic Queries
There are several key areas which require improvement, most of which have arisen during the evolution of RTPA → Eventcube and subsequent progression and growth of the system.

### Multiple Event Checkout
Multiple event checkout causes a number of complications throught the system, particularly for reporting.

Take the following order as an example for an order on the *MOTR EVNT* store.

<pre>
# Customer Costs
┏━━━━━┳━━━━━━━━━━━━━━━━━┳━━━━━━━━━━┳━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━┳━━━━━━━━┳━━━━━━━━━━━━━┳━━━━━━━━━━━━┓
┃ Qty ┃ Ticket          ┃ Event    ┃ Promoter       ┃ Type           ┃ Unit   ┃ Booking Fee ┃ Line Total ┃
┡━━━━━╇━━━━━━━━━━━━━━━━━╇━━━━━━━━━━╇━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━╇━━━━━━━━╇━━━━━━━━━━━━━╇━━━━━━━━━━━━┩
│ 2   │ Early Bird      │ Rockfest │ YZX Promotions │ By Post        │ £35.00 │ £3.50       │     £77.00 │
│ 1   │ General Release │ Submerge │ Whitman Events │ Download       │ £18.00 │ £1.80       │     £19.80 │
├─────┼─────────────────┼──────────┼────────────────┼────────────────┼────────┼─────────────┼────────────┤
│ 1   │ Social discount │ Rockfest │                │ Twitter        │ -£0.50 │             │     -£0.50 │
└─────┴─────────────────┴──────────┴────────────────┴────────────────┼────────┴─────────────┼────────────┤
                                                                     │ Tickets              │     £88.00 │
                                                                     │ Booking Fees         │      £8.80 │
                                                                     │ Delivery             │      £1.50 │
                                                                     │ Discounts            │     -£0.50 │
                                                                     ├──────────────────────┼────────────┤
                                                                     │ Total                │     £97.80 │
                                                                     └──────────────────────┴────────────┘

# System Costs
┌───────────────────────────┬────────────┐
│ Card fee   (@ 2.4% + 20p) │ £2.55      │
│ System fee (@ 3%)         │ £2.93      │
└───────────────────────────┴────────────┘
</pre>

The total amount recieved via the payment gateway is `£95.25`. Eventcube takes `£2.93` leaving `£92.32` for MOTR EVNT.

The complexities are introduced when the Store amount is divvied up for each Event and Promoter.

The Promoter's cut is defined as `price to customer - shipping - booking`, in the above case *General Release* is a Download ticket — there is no postage fee to deduct. _Whitman Events_ should therefor take `£18.00`, _YZX Promotions_ should take `£68.50`.

Calculating reporting per-Event is more complex. All we know from the database is that, on this individual order there was a discount of £0.50. We don't know which event it was applied to. To achieve a total, we need to spin through each order individually to figure out which event (or ticket) the discount was applied to. This can be extremely time consuming if there are thousands of orders and/or discounts.

The same issue applies to *delivery costs*, *card fees* and *system fees*, we know what the _total_ amounts were for the order, but not per-Event. We can't simply divvy amounts up equally for each because the ratio between ticket prices vary; in our example, Rockfest tickets are 388% more than Submerge tickets. Card fees to purchase tickets on their own: `(19.80 * 0.024) + 0.2 = £0.68` vs `(77.00 * 0.024) + 0.2 = £2.05`.

### Promoter Model
The current model of an Event belongs to a Promoter, Promoter belongs to a Store no longer fits with the Eventcube model. The majority of Eventcube Stores have only one Promoter, the Store owner **is** the promoter. Reports showing revenue distributed between Eventcube, Store and Promoter is therefor no longer useful and causes confusion for Stores not utilising the Promoter model.

A wise move forward is to drop the Promoter Model and use a hasMany relationship. An Event should belong to a Store, but it could still have one (or many) Promoters _attached_ to it, but the Event is no longer reliant on them.
