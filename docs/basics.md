# Basics

- [Artisan Console](#artisan)
- [RSN Service Provider](#rsn-service-provider)
- [Stylesheets](#stylesheets)
- [Theming](#theming)
- [Authentication](#authentication)
- [Email](#email)
- [Queues](#queues)
- [Deployment](#deployment)

## Artisan
Laravel ships with a command line tool called <a href="https://github.com/laravel/docs/blob/4.1/commands.md">Artisan</a>. Artisan allows you to run scripts and tasks via the console or a cron job. You can list all commands using the `php artisan list` command.

Custom commands are stored within `/app/commands` and must be registered in `/app/start/artisan.php`.

Commands may also be fired from within the application using the `Artisan::call()` facade.

## RSN Service Provider
An instance of `Rsn\Rsn` is registered when the application is loaded.

The `Rsn` class is used to encapsulate the current store the user is currently accessing and can be accessed using the facade `RSN::getStore()`.

## Stylesheets
### Eventcube Admin Stylesheets
A very basic start has been made to convert stylesheets for **Eventcube Admin** from CSS to SCSS to allow for manageable and scalable development.

The initial purpose for this change was to reduce the number HTTP requests to the server. We have compiled all vendor and system stylesheets into one main css file `/public/assets/css/ec-admin.css`, alongside `/public/assets/css/ec-admin-hd.css` which offers several higher resolution images to provide a higher fidelity appearance for retina/high pixel density devices.

The file structure is based upon the principles set out on [Sass-Guidelin.es: Architecture](http://sass-guidelin.es/#architecture). At present there are only several component groups that have been split out into their own files (those most recently developed). It is our recommendation that `/resources/assets/sass/layout/_main.scss` and `/resources/assets/sass/layout/_style.scss` would be broken down into component and page groups to fit with this file structure.

### SCSS Workflow
The new main syntax (as of Sass 3 - Syntactically Awesome Style Sheets) is known as “SCSS” (for “Sassy CSS”), and is a superset of CSS3's syntax. This means that every valid CSS3 stylesheet is valid SCSS as well. SCSS files use the extension .scss. SCSS files are compiled (allowing you to minify) into in CSS before runtime – this can either be done on the server or locally before deployment.

The SCSS is compiled locally to allow effcient development & testing. This gives reassurance that all stylesheets will be correct once deployed.

To compile SCSS you should run [LiveReload](http://livereload.com) on a local machine to monitor the project directory for file changes, which in turn compiles our CSS on the fly. This teamed up with the [LiveReload Browser Extensions](http://livereload.com/extensions/) will provide you with automatic refreshes on each CSS change to make your workflow more efficient.

It is recommended to move over to using SCSS to write the CSS in both **Eventcube Admin** and **Eventcube Front** due to the shear size of the two applications. SCSS allows you to break all of your styles into shorter more manageable files from a development perspective, whiles serving up a single CSS file to the end user.

> **Note:** There are other ways to compile SCSS, both via the command line and other applications to best suit how you work. If using SCSS doesn't fit with your current workflow, it will be very simple for us to remove the SCSS structure and revert to pure CSS if required.

#### Setting up LiveReload
To set up LiveReload for use with both applications&hellip;

1. Run the LiveReload application.
2. Click on the "**&plus;**" bottom-left of the window and select the root directory for the project _(this will be the directory LiveReload will monitor for changes)_.
3. Click "**Options**" next to "_Monitoring XX file extensions._"
4. Add `twig` to "_Additional_", add `app/storage` to "_Folders to exclude_" and click "**Apply**".
5. Make sure the checkbox next to "_Compile SASS, LESS, Stylus, CoffeeScript and other_" is **checked** and click "**Options**".
6. In "**Output Paths**" you will see all `.scss` source files in the project along with their Output path. Most of this should be imported into the main files as noted below. Click "**Apply**".
7. In "**Compiler Settings**" make sure "_SASS_" is **enabled** and choose "**Compressed output style**" from the dropdown.
8. Everytime a file in the project directory is saved, _LiveReload_ will compile the CSS.
9. In your browser turn on _Live Reload_ and you should now see all SCSS style changes refresh as you save the file.

> **Note:** The only SCSS files that need to be compiled into CSS are `resources/assets/sass/ec-admin-hd.scss` and `resources/assets/sass/ec-admin.scss` on **Eventcube Admin**. All of the other files are imported into these two files CSS files and should have _Output Paths_ of `public/assets/css/ec-admin-hd.css` and `public/assets/css/ec-admin.css` respectively.


### Eventcube Front Stylesheets
Vendor and system CSS stylesheets are located in `/public/assets/css`. These are currently all being loaded individually on the front-end, but should be moved across to the same SCSS architecture as the **Eventcube Admin**.

The stylesheet `/public/assets/css/layout.css` is responsible for the stucture and layout of the front end whilst `/public/assets/css/style.css` is resposible for the aesthetic and visual appearance of the front-end of the system.

#### Theme Colour
The _Theme Colour_ which can be set in _Customise Store_ within the **Eventcube Admin** is injected as a `<style>` tag into `app/views/_stores/style_custom.twig` which is then included in the `/app/views/layouts/master.twig` template. This overrides all primary `color`, `border-color` and `background-color` selectors across the whole of **Eventcube Front**.

### Overriding CSS
Please see [Overriding Stylesheets & Assets](basics.md#overriding-stylesheets-amp-assets) below.

## Theming
### Templates
Stores on **Eventcube Front** utilise a base set of templates within the `/app/views` directory. See the [templating documentation](getting-started.md#templates) for further details.

Individual template files can be overridden on a Store-by-Store basis. Within `/app/views/_stores`, a directory should be created using the Store identifier.

The structure within the Store's template override directory should match that of the base theme. E.g. to override the FAQ page for a store, create the file: `/app/views/_stores/[identifier]/pages/support/faq.twig`.

Child template files (i.e. not the base template; `layouts/master.twig`) should extend the base template using the `helper_extend()` function to check if the current Store has an overridden base template.

```twig
{% extends helper_extend('layouts.master') %}
```
### Overriding Stylesheets & Assets
Theme specific assets should be stored in the `/public/assets/themes` directory on **Eventcube Front**, once again using the Store identifier. The `themeExists()` function can verify if a specific file exists for the current Store. The asset url can be loaded using the Store's `theme()` function.

When developing custom styles it is strongly recommended that you create a `custom.css` file that simply overrides the default stylesheets.

```twig
{% if store.themeExists('custom.css') %}
<link rel="stylesheet" href="{{ store.theme('custom.css') }}" media="screen,projection" />
{% endif %}
```

> **Note:** If the store identifier is modified, the template override directory names must also be changed.

## Authentication
[Authentication and ACL](structure.html#roles) is handled by the [`cartalyst/sentry`](https://packagist.org/packages/cartalyst/sentry) package. Official documentation is available [here](https://cartalyst.com/manual/sentry/2.1).

> **Note:** Error reporting is logged using a service also called [Sentry](https://www.getsentry.com) and is unrelated to the Cartalyst Sentry authentication package.

## Email
### Sending Email
Email is sent using the [Mandrill](https://mandrillapp.com) API through the [`mandrill/mandrill`](https://packagist.org/packages/mandrill/mandrill) package.

All emails are sent and queued via the `App\Libraries\Mailer` class rather than Laravel's built in Mail class which does not support Mandrill. Emails can be forced to bypass the queue by using the `force()` command.

### Email Override
An override address can be added to the configuration property `rsn.email.override`. Adding this property will route all emails sent via the **Eventcube Admin** application to the specified address.

## Queues
> **Note:** **Eventcube Admin** is the queue worker application. It processes queue jobs for itself and **Eventcube Front**.

The queue controller is [Amazon SQS](http://aws.amazon.com/sqs/) and makes use of the `aws/aws-sdk-php` package (version [`~2.4`](https://packagist.org/packages/aws/aws-sdk-php#2.4.12)).

The queue worker runs using the `queue:listen` console command:

```sh
php artisan queue:listen --queue=default --timeout=0 --tries=3 --delay=10
```

The queue worker is kept alive using [Supervisor](http://supervisord.org/) (version `3.0`) and should be configured with:

```sh
# /etc/supervisor/conf.d/ecstage.conf

[program:ecstagequeue]
command=php artisan queue:listen --queue=default --timeout=0 --tries=3 --delay=10
directory=/var/www/eventcube-admin
stdout_logfile=/var/www/eventcube-admin/app/storage/logs/supervisor.log
redirect_stderr=true
```

> **Note:** Queued jobs can be in the form of a closure or a service. Queue services are located in `/app/queues`.

## Deployment
All applications are deployed using [Envoyer](http://envoyer.io).
