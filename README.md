# Eventcube Documentation

- [Getting Started](docs/getting-started.md)
- [Basics](docs/basics.md)
- [Structure](docs/structure.md)
- [Orders](docs/orders.md)
- [Integration](docs/integration.md)
- [Miscellaneous](docs/misc.md)


_28th August 2015 Version 1.0_

> **Note:** HTML Documentation can be generated for these docs using [couscous](GENERATOR.md).
